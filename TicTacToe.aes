contract TicTacToe =

  record state = { player0: address,    // Address of player 1 (plays as X)
                   player1: address,    // Address of player 2 (plays as O)
                   turn: int,           // Always either 0 or 1, depending on which player's turn it is (X and O respectivelly)
                   winner: int,         // Set to 0 or 1 depending on who won (X or O respectivelly). Set to -1 while the game is ongoing.
                   grid: map(int, int), // Holds the value of each square of the Tic Tac Toe grid. Values 0 and 1 for X, O and empty respectivelly
                   height: int,         // Height of the Tic Tac Toe grid
                   width: int,          // Width of the Tic Tac Toe grid
                   n: int }             // How many in a row a player must have in order to win the game
  

  stateful entrypoint init(player0: address, player1: address, width: int, height: int, n: int) =
    /** Basic constructor, only initiates the state */

    { player0 = player0,
      player1 = player1,
      turn = 0,            // Player 1 plays first (as X)
      winner = -1,         // There is no winner yet
      grid = {},           // The grid is empty at the start of the game
      height = height,
      width = width,
      n = n }
  

  function validCoordinate(x: int, y: int) : bool = 
    /** Function checks whether the supplied coordinate is within the grid. */
    (x >= 0) && (x < state.width) && (y >= 0) && (y < state.height)
  

  function getGridValue(x: int, y: int) : int = 
    /** Function returns the value of the grid at coordinates x,y. */
    state.grid[x + y*state.width = -1]

  stateful entrypoint move(x: int, y: int) = 
    /** Function allows the players to make their move by placing their symbol (X or O) on the x,y location on the grid */

    let turn = state.turn

    // Ensure that it is the players turn, by checking whether the player sent this transaction
    require((Call.caller == state.player0 && turn == 0) || (Call.caller == state.player1 && turn == 1), "It is not your turn!")

    // Ensure that the game is still ongoing (nobody won yet)
    require(state.winner == -1, "This game is over!")

    // Ensure that the location where the player is placing the symbol is within the grid
    require(validCoordinate(x, y), "Point is outside the grid bounds!")

    // Ensure that the location is still free
    require(getGridValue(x, y) == -1, "This location is already occupied!")
    
    // Save the move in the grid and update the 'turn' to be the other player's turn
    put(state{turn @ t = (t + 1) mod 2, grid[x + y*state.width] = turn})
    
    // Check whether this move was the winning move and if so update the state accordingly
    if (didWin(turn, x, y))
      put(state{winner=turn})


  function didWin(turn: int, x: int, y: int) : bool =
    /**
      Function checks whether the move the player made by placing their symbol (X or O) on x,y is a game winning move.
      It does so by counting the amount of symbols of that player along the column, row and diagonals, which cross
      at the newly placed symbol.
      */
    let traces: list(int) = [ trace(turn, x - 1, y    , -1,  0) + trace(turn, x + 1, y    , 1,  0) + 1, // Row
                              trace(turn, x    , y - 1,  0, -1) + trace(turn, x    , y + 1, 0,  1) + 1, // Column
                              trace(turn, x - 1, y - 1, -1, -1) + trace(turn, x + 1, y + 1, 1,  1) + 1, // First Diagonal
                              trace(turn, x - 1, y + 1, -1,  1) + trace(turn, x + 1, y - 1, 1, -1) + 1  // Second Diagonal
                            ]

    // At this point the 'traces' variable holds the amount of consecutive symbols the current player has,
    // which cross over the newly placed symbol. If either of these is greater than or equal to state.n, then the
    // player wins.
    max(traces) >= state.n


  function trace(turn: int, x: int, y: int, dx: int, dy: int) = 
    /** Recursive function counts the amount of elements of state.grid equal to 'turn' starting from x,y along dx,dy */
    if (turn == getGridValue(x, y))
      1 + trace(turn, x + dx, y + dy, dx, dy)
    else
      0


  function max(nums: list(int)) : int = 
    /**
      Function finds the greatest number in a list of numbers via recursion on the list.
      Warning: Assumes that 0 is the smallest number possible.
      */
    switch(nums)
      [] => 0
      n::nums => greatest(n, max(nums))


  function greatest(a: int, b: int) : int = 
    /** Function simply returns the greater of the two numbers */
    if(a>b) a else b
  

  // ******************************************************************************************
  // *  The following are only utility functions for reading the contract data from outside.  *
  // ******************************************************************************************

  entrypoint getGrid(x: int, y: int) : int = 
    getGridValue(x, y)
    
  entrypoint getTurn() : int = 
    state.turn
    
  entrypoint getWinner() : int = 
    state.winner

